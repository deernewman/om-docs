
# om-collection镜像包

镜像文件版本与对应的功能修改：

* 0.0.8 增加star 时间线
* 0.0.9 update python version 3.5 -> 3.7
* 0.0.10 支持获取gitee 组织star用户列表
* 0.0.11 支持maillist数据通过外网域名获取
* 0.0.12 组织star 增加时间线
* 0.0.15 增加Committer、gitee download、maillist总量数据展示，新增pypi数据源